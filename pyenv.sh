#!/bin/sh
if [ -f ./venv ]; then
    echo 'Python venv exists'
    echo 'activate venv'
    . ./venv/bin/activate
    echo 'install requirements'
    pip3 install -r requirements.txt

else
    echo 'create venv'
    python3 -m venv ./venv
    echo 'activate venv'
    . ./venv/bin/activate
    echo 'insall requirements'
    pip3 install -r requirements.txt
fi






